﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTOLAI
{
    class VTOLEngineLFE : PartModule
    {
        /// <summary>
        /// Sees if there's a ModuleEngines attached to this part, if it is, it will continue to set up this class.
        /// </summary>
        public override void OnActive()
        {
            Part basePart = base.part;

            PartModuleList modules = basePart.Modules;

            ModuleEngines engineModule = (ModuleEngines) modules["ModuleEngines"];

            if (engineModule == null)
            {
                print("Couldn't find a engine module attached to me");
            }

            base.OnActive();
        }
    }
}
